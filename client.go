package omdbapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type Client struct {
	HTTPClient *http.Client
	apiKey     string
}

func NewClient(apiKey string) *Client {
	return &Client{http.DefaultClient, apiKey}
}

func (c *Client) baseUrl() string {
	return "http://www.omdbapi.com/?apikey=" + c.apiKey
}

func (c *Client) DownloadPoster(title string) error {

	movie, err := c.SearchImdbByName(title)
	if err != nil {
		return err
	}

	if movie.Poster == "N/A" {
		fmt.Printf("%s do not have poster to download.\n.", title)
		return nil
	}

	fmt.Printf("Downloading Poster from url: %s to %s\n", movie.Poster, movie.Title)

	response, err := http.Get(movie.Poster)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	f_name := strings.ToLower(strings.ReplaceAll(movie.Title, " ", "_")) + ".jpg"

	output, err := os.Create(f_name)
	if err != nil {
		return err
	}
	defer output.Close()
	n, err := io.Copy(output, response.Body)
	if err != nil {
		return err
	}
	fmt.Println(n, "bytes downloaded.")

	return nil
}

func (c *Client) SearchImdbByName(title string) (*ImdbResponse, error) {

	t := url.QueryEscape(title)
	url := c.baseUrl() + "&t=" + t
	fmt.Printf("Searching URL: %s\n", url)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Pesquisa Falhou: %s / %s", url, resp)
	}

	var result ImdbResponse
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, err
	}

	if result.Response == "False" {
		return nil, fmt.Errorf("Movie/Serie not found. Check the input and try again")
	}

	return &result, nil
}
