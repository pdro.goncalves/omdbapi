(I)OMDB GO API Client
===============================

Overview
--------

A GO package to interact with [The Open Movie Database (OMDB) API](http://www.omdbapi.com/).


Installation
----------

```shell
go get gitlab.com/pdro.goncalves/omdbapi
```

Usage
--------------

    ```
    package main

    import (
        "fmt"
        "omdbapi"
    )

    func main() {

        k := "" // <--------- PUT YOUR API KEY HERE
        client := omdbapi.NewClient(k)
        title := "The Mandalorian"

        fmt.Printf("Searching by Movie: %s\n", title)

        m, err := client.SearchImdbByName(title)
        if err != nil {
            fmt.Printf("Error searching title: %s", err)
        }

        fmt.Printf("Downloading title %s poster\n", title)
        err = client.DownloadPoster(title)
        if err != nil {
            fmt.Printf("Error downloading title poster: %s", err)
        }

    }
```
